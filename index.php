<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="./semantic/dist/semantic.css">
    <script
        src="./node_modules/jquery/dist/jquery.min.js"></script>
        <script src="./semantic/dist/semantic.js"></script>
    <title>Document</title>
</head>
<body>
    <h1>Computer science figure</h1>
    
    

    <?php

    $row = 0;
    if (($handle = fopen("cs_figures.csv", "r")) !== FALSE) {
        echo "<div class='ui link cards'>";
        while (($data = fgetcsv($handle, ",")) !== FALSE) {
            $num = count($data);
            if($data[0] != "name") {
                echo "
                    <div class='card'>
                        <div class='image'>
                            <img src='" . $data[5] . "'>
                        </div>
                        <div class='content'>
                            <div class='header'>$data[0]</div>
                            <div class='meta'>
                                <a>$data[2]</a>
                            </div>
                            <div class='description'>$data[3]</div>
                        </div>
                        <div class='extra content'>
                            <span class='right floated'>Born in $data[1]</span>
                        </div>
                    </div>";
            }
            $row++;
        }
        echo "</div>";
        fclose($handle);
    }

    ?>


</body>
</html>
